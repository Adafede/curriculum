== icon:user[] Personal Details

image:../img/pic.png[pic,125,125,float="right",align="right"]

=== Contact

icon:envelope-o[] mailto:adriano.rutz@ik.me[adriano.rutz@ik.me] {nbsp} {nbsp} icon:phone[] +41 76 521 45 30 +
icon:map-marker[] Geneva, Switzerland

=== Social Media

icon:linkedin[] https://www.linkedin.com/in/adrianorutz/[https://www.linkedin.com/in/adrianorutz/]  {nbsp} {nbsp} icon:twitter[] https://twitter.com/Adafede[https://twitter.com/Adafede] +
icon:github[] https://github.com/Adafede[https://github.com/Adafede]  {nbsp} {nbsp}
icon:gitlab[] https://gitlab.com/Adafede[https://gitlab.com/Adafede]

=== Bio

****
*Adriano Rutz* is a pharmacist.
After working with volatiles (GC-MS/GC-O) in the Analytical Research Laboratory and Research & Development department of Tradall SA (Bacardi group), he started a PhD in Phytochemistry in Prof. Wolfender’s group.
He is now developing advanced LC-MS-based metabolite profiling methods and tools to assess phytochemical and sensorial profiles of complex plant extracts.
****
