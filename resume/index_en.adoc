= Adriano Rutz's Résumé
:icons: font
:stylesheet: ../css/adoc-github-modified.css
:toc: left

Last update: {localdate}

Download the link:https://gitlab.com/Adafede/curriculum/-/blob/main/pdf/adriano-rutz-resume-en.pdf[printable (PDF) version]. +

include::personal-details/index_en.adoc[]

include::technical-skills/resume_en.adoc[]

include::professional-experience/index_en.adoc[]

include::education/resume_en.adoc[]

// include::certifications/resume_en.adoc[]

include::technical-writing/resume_en.adoc[]

include::conferences-and-presentations/resume_en.adoc[]

include::repositories/resume_en.adoc[]

== icon:plus[] Recommandations

Available mailto:adriano.rutz@ik.me[on request].
