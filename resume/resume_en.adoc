:icons: font
:stylesheet: ../css/adoc-github-modified.css

include::personal-details/resume_en.adoc[]

include::technical-skills/resume_en.adoc[]

include::professional-experience/resume_en.adoc[]

include::education/resume_en.adoc[]
