=== Language Skills

- *French*: native language
- *English*: upper intermediate (C1)
- *German*: upper intermediate (C1) good knowledge of _Swiss-German_
- *Italian*: native language
