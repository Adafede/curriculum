# Environment Variable Defaults
export NAME       	?= adriano-rutz-resume-en
export SOURCEDIR  	?= resume
export SOURCE_HTML  ?= $(SOURCEDIR)/index_en.adoc
export SOURCE_PDF   ?= $(SOURCEDIR)/resume_en.adoc
export OUTDIR_HTML  ?= public
export OUTDIR_PDF  	?= pdf
export PAGE_SIZE  	?= A4

# Utility
export HTMLOUT    := $(OUTDIR_HTML)/index.html
export PDFOUT     := $(OUTDIR_PDF)/$(NAME).html
export TRUEPDFOUT     := $(OUTDIR_PDF)/$(NAME).pdf

.PHONY: help html pdf 

help:
	@echo 'Resume/CV - Turn text into professional PDF or HTML resume/CV'
	@echo ''
	@echo 'Usage: make <action>'
	@echo ''
	@echo 'Actions:'
	@echo '  html       to make a standalone HTML version of the resume'
	@echo '  pdf        to produce a PDF version of the resume'
	@echo ''
	@echo 'Environment variables'
	@echo '  NAME       the filename (without extension) of the output'
	@echo '             (currently: $(NAME))'
	@echo '  SOURCE_HTML     the source file to use as input'
	@echo '             (currently: $(SOURCE_HTML))'
	@echo '  SOURCE_PDF     the source file to use as input'
	@echo '             (currently: $(SOURCE_PDF))'
	@echo '  PAGE_SIZE  the page size for the PDF (example: Letter)'
	@echo '             (currently: $(PAGE_SIZE))'
	@echo ''
	@echo 'Example:'
	@echo '  $$ make pdf'

html:
	asciidoctor \
		-a nofooter \
		-o $(HTMLOUT) \
		$(SOURCE_HTML)

pdf:
	asciidoctor \
		-a pdf-page-size=$(PAGE_SIZE) \
		-o $(PDFOUT) \
		$(SOURCE_PDF)
	-wkhtmltopdf --enable-local-file-access $(PDFOUT) $(TRUEPDFOUT) 
	rm $(PDFOUT)
